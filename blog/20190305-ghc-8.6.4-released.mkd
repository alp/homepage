---
author: Ben Gamari
title: "GHC 8.6.4 released"
date: 2019-03-05T20:52:14
tags: release
---

The GHC team is very happy to announce the availability of GHC 8.6.4, a
bugfix release in the GHC 8.6 series. The source distribution, binary
distributions, and documentation for this release are available at
[downloads.haskell.org](https://downloads.haskell.org/~ghc/8.6.4).

The 8.6.4 release fixes several regressions present in 8.6.3 including:

 - A regression resulting in segmentation faults on Windows introduced
   by the fix for #16071 backported in 8.6.3. This fix has been reverted,
   meaning that 8.6.4 is once again susceptible to #16071. #16071 will
   be fixed in GHC 8.8.1.

 - A bug resulting in incorrect locking on Darwin, potentially resulting
   in hangs at shutdown (#16150)

 - A few bugs in the profiled runtime resulting in the potential for
   memory unsafety has been fixed (#15508).

 - The `process` and `transformers` libraries shipped properly reflect
   released Hackage releases (#16199)

 - A bug where TH name resolution would break in a plugin context has
   been fixed (#16104)

 - Compilers that do not support TemplateHaskell no longer advertise
   such support in `--supported-languages` (#16331)

As a few of these issues are rather serious users are strongly
encouraged to upgrade. See
[Trac](https://ghc.haskell.org/trac/ghc/query?status=closed&milestone=8.6.4&col=id&col=summary&col=status&col=type&col=priority&col=milestone&col=component&order=priority)
for a full list of issues resolved
in this release.

Note that this release ships with one significant but long-standing bug
(#14251): Calls to functions taking both Float# and Double# may result
in incorrect code generation when compiled using the LLVM code generator.
This is not a new issue, it has existed as long as the LLVM code
generator has existed; however, changes in code generation in 8.6 made
it more likely that user code using only lifted types will trigger it.

Note also that this is the first release cut from our (yet again)
revamped continuous integration infrastructure. While we have done a
great deal of checking to ensure that the build configuration reflects
that of previous releases, do let us know if something looks off.

Happy compiling!

